
public class Sort {

	public static int[] sort(int[] array){
		int[] sortedArr = array;
		if (array.length < 2) return array;
		for (int i = 0; i < sortedArr.length - 1; i++) {
			for (int j = 0; j < sortedArr.length - i - 1; j++) {
				if (sortedArr[j] > sortedArr[j + 1]) {
					int temp = sortedArr[j];
					sortedArr[j] = sortedArr[j + 1];
					sortedArr[j + 1] = temp;
				}
			}
		}
		return sortedArr;
	}
}
