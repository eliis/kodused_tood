


import static org.junit.Assert.*;

import org.junit.Test;

public class PositiveEvenNumbersSumTest {

	@Test
	public void whenOnePositiveEvenNumber() {
		assertEquals("Unexpected result", 2, PositiveEvenNumbersSum.sum(new int[] {2}));
	}

	@Test
	public void whenMultiplePositiveEvenNumbers() {
		assertEquals("Unexpected result", 12, PositiveEvenNumbersSum.sum(new int[] {2, 4, 6}));
	}

	@Test
	public void whenNegativeEvenNumbers() {
		assertEquals("Unexpected result", 12, PositiveEvenNumbersSum.sum(new int[] {-2, -4, 2, 4, 6}));
	}
	
	@Test
	public void whenUnevenNumbers() {
		assertEquals("Unexpected result", 12, PositiveEvenNumbersSum.sum(new int[] {1, 2, 4, 6}));
	}
	
	@Test
	public void whenNegativeAndUnevenNumbers() {
		assertEquals("Unexpected result", 12, PositiveEvenNumbersSum.sum(new int[] {-2, -4, 1, 2, 4, 6}));
	}
}
