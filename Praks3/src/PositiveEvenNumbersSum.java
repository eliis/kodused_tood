
public class PositiveEvenNumbersSum {
	
	public static int sum(int[] array) {
		int sum = 0;
		for (int number : array) {
			if (number > 0 && number % 2 == 0) {
				sum += number;
			}
		}
		return sum;
	}
}
