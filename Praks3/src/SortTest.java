

import static org.junit.Assert.*;

import org.junit.Test;


public class SortTest {

	@Test
	public void whenEmptyArray() {
		assertArrayEquals("Unexpected result",  new int[] {}, Sort.sort(new int[] {}));
	}

	@Test
	public void whenOneNumberInArray() {
		assertArrayEquals("Unexpected result",  new int[] {1}, Sort.sort(new int[] {1}));
	}
	
	@Test
	public void whenTwoNumbersInOrder() {
		assertArrayEquals("Unexpected result",  new int[] {1, 4}, Sort.sort(new int[] {1, 4}));
	}
	
	@Test
	public void whenTwoNumbersUnordered() {
		assertArrayEquals("Unexpected result",  new int[] {1, 4}, Sort.sort(new int[] {4, 1}));
	}
	
	@Test
	public void whenThreeNumbersInOrder() {
		assertArrayEquals("Unexpected result",  new int[] {1, 3, 6}, Sort.sort(new int[] {1, 3, 6}));
	}
	
	@Test
	public void whenThreeNumbersUnordered() {
		assertArrayEquals("Unexpected result",  new int[] {1, 3, 6}, Sort.sort(new int[] {6, 1, 3}));
	}
	
	@Test
	public void whenMultipleNumbers() {
		assertArrayEquals("Unexpected result", new int[] {1, 4, 6, 8, 23, 34, 36, 56, 66},
				Sort.sort(new int[] {4, 23, 34, 1, 6, 66, 56, 8, 36}));
	}
}
